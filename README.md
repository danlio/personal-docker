# 0. 使用需求

 - 安裝 docker , docker-compose

 - git clone 專案下來

```bash
 - git clone ssh://git@gitlab-ci.prod:10022/system-managers/personal-docker.git

 - cd personal-docker

```

# 1. PHP Laravel
## docker + redis + mysql + phpmyadmin + php7 + clickhouse

```bash
  1. 將.env 修改成自己的
    - cp .env.example .env
    - vi .env
    # 將 .env 內 PROJECT_PHP= 打上自己專案路徑,儲存後離開

  2. 再personal-docker資料夾底下執行
    # 啟用環境
    - docker-compose -f php-laravel.yaml up -d
    # 關閉環境
    - docker-compose -f php-laravel.yaml down

  3. 環境已完成

  4. vue js
    # 第一次使用請針對專案 install
    - docker exec -i php7 sh -c "cd {project name} && npm install"
    # 編譯執行所有 Mix 任務和壓縮輸出
    - docker exec -i php7 sh -c "cd {project name} && npm run production"
    # 即時監控資源的變化
    - docker exec -i php7 sh -c "cd {project name} && npm run watch-poll"
```

# 2. NodeJs Pixi
## NodeJs + Pixi

```bash
  1. 將.env 修改成自己的
    - cp .env.example .env
    - vi .env
    # 將 .env 內 PORJECT_NODE= 打上自己專案路徑,儲存後離開

  2. 再personal-docker資料夾底下執行
    # 啟用環境
    - docker-compose -f node-pixi.yaml up -d
    # 關閉環境
    - docker-compose -f node-pixi.yaml down

  3. 環境已完成
```

# 3. php + nginx
## php + nginx

```bash
  1. 將.env 修改成自己的
    - cp .env.example .env
    - vi .env
    # 將 .env 內 PORJECT_NODE= 打上自己專案路徑,儲存後離開

  2. 再personal-docker資料夾底下執行
    # 啟用環境
    - docker-compose -f php.yaml up -d
    # 關閉環境
    - docker-compose -f php.yaml down

  3. 環境已完成
```

# 4. env 範例

```bash
# PHP env

PROJECT_PHP=/Users/danlio_hsu/Project
PHP_FPM_PORT=9090
PMA_PORT=8080
MYSQL_USER=danlio
MYSQL_PWD=root

# phpMyadmin
MAX_EXECUTION_TIME=600
# upload_max_filesize & post_max_size
UPLOAD_LIMIT=2048K
MEMORY_LIMIT=512M

# MySql
MYSQL_PORT=3306

# Clickhouse
CH_USER=root
CH_PORT=9000
CH_HTTP_PORT=8123

# NODEJS env
PORJECT_NODE=/Users/danlio_hsu/Project/bbpGame


```

## Domain

- 須綁host


|                 | PHP                         | Node                   |
|:--------------- |:--------------------------- |:---------------------- |
|  Demo           | http://local.cs.rd3/        |                        |
|  front          | http://local.front.rd3/     |                        |
|  gcp-api        | http://local.gcp-api.rd3/   |                        |
|  gcp-web        | http://local.gcp-web.rd3/   |                        |
|  gcp-front      | http://local.gcp-front.rd3/ |                        |
|  pma            | http://local.pma.rd3/       |                        |
|  clickhouse UI  | http://local.ch-tabix.rd3/  |                        |
|  node-server    |                             | http://local.pixi.rd3/ |

## Clickhouse UI

- login 資訊

```
name : 自己定義...
host : 127.0.0.1:8123
user : default
pwd  : 不用密碼...
```
![image](/uploads/c40fe211df3d493feb04c0a7590a3aeb/image.png)

## Command 發生任何問題先執行這個就對了

```
#關閉掉全部 Exited 狀態的 docker

./command.sh
```
