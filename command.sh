#!/bin/bash

if (! docker stats --no-stream );
then
  echo ""
  echo "         Are you kidding me? Docker hasn't started at all!"
  echo "       /¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯"
  echo "¯\_(ツ)_/¯"
else
  #關閉掉全部 Exited 狀態的 docker
  count=$(docker ps -a | grep Exited | wc -l)
  if [ $count != 0 ]
  then
    echo "docker Exited count " $count
    docker rm $(docker stop $(docker ps -a | grep Exited | awk '{print $1}'))
  else
    echo 'docker status Exited is 0'
  fi
fi



