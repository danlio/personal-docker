ATTACH TABLE _ UUID 'b150c3fb-43be-4874-aace-a1fd17c75ccc'
(
    `event_date` Date,
    `event_time` DateTime,
    `name` String,
    `value` Float64
)
ENGINE = MergeTree
PARTITION BY toYYYYMM(event_date)
ORDER BY (event_date, event_time)
SETTINGS min_bytes_for_wide_part = '10M', index_granularity = 8192
