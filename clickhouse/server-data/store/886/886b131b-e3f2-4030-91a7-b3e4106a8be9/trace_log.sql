ATTACH TABLE _ UUID '821d80c9-071a-4f2a-8827-173dd5eb2a47'
(
    `event_date` Date,
    `event_time` DateTime,
    `timestamp_ns` UInt64,
    `revision` UInt32,
    `trace_type` Enum8('Real' = 0, 'CPU' = 1, 'Memory' = 2, 'MemorySample' = 3),
    `thread_id` UInt64,
    `query_id` String,
    `trace` Array(UInt64),
    `size` Int64
)
ENGINE = MergeTree
PARTITION BY toYYYYMM(event_date)
ORDER BY (event_date, event_time)
SETTINGS min_bytes_for_wide_part = '10M', index_granularity = 8192
